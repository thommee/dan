﻿var appControllers = angular.module('appControllers', []);

appControllers.controller('GalleryListCtrl', ['$scope', '$http', 'Gallery', function ($scope, $http, Gallery) {

    $scope.galleries = Gallery.query();

    $http.get('json/sortList.json').success(function (data) {
        $scope.sortList = data;
    });

    $scope.orderProp = 'when';

}]);

appControllers.controller('GalleryDetailCtrl',
    ['$scope', '$routeParams', '$http', 'Gallery', '$timeout', function ($scope, $routeParams, $http, Gallery, $timeout) {

    $scope.galleryId = $routeParams.galleryId;

    $scope.gallery = Gallery.get({galleryId: $routeParams.galleryId}, function(gallery) {
        $scope.loadedPhoto = gallery.photos[0];
    });

    $scope.imgClassName = 'fadeIn';
    $scope.loadPhoto = function(photo) {
        $scope.imgClassName = 'fadeOut';
        $scope.loadedPhoto = photo;
        $timeout(function(){ $scope.imgClassName = 'fadeIn'; }, 0);
    }

}]);

appControllers.controller('CommentsCtrl',
    ['$scope', '$localStorage', '$routeParams', 'Comment', function ($scope, $localStorage, $routeParams, Comment) {


    $scope.galleryId = $routeParams.galleryId;
    $scope.commentsJSON = Comment.get({ galleryId: $scope.galleryId });

    $scope.comments = $localStorage.comments || $scope.commentsJSON;

    $scope.myComments = function() {
        return $scope.comments.filter(function(comment) {
            return comment.galleryId == $scope.galleryId;
        });
    };

    $scope.addComment = function(galleryId){
        //pola spoza formularza
        $scope.comment.galleryId = galleryId;
        $scope.comment.date = new Date();
        //wstawiamy do listy
        $scope.comments.push($scope.comment);
        //czyścimy formularz
        $scope.comment = "";

        $localStorage.comments = $scope.comments;
    };

    $scope.removeComment = function(commentId) {
        $scope.comments.splice(commentId, 1);
        $localStorage.comments = $scope.comments;

        if (!$scope.comments.length) {
            $localStorage.$reset();
        }
    };

    $scope.clearComments = function() {
        $localStorage.$reset();
        $scope.comments = [];
    }
}]);

