angular.module('appFilters', [])
    .filter('monthName', function() {
            return function(monthNumber) {
                var monthNames = [
                    'Styczeń',
                    'Luty',
                    'Marzec',
                    'Kwiecień',
                    'Maj',
                    'Czerwiec',
                    'Lipiec',
                    'Sierpień',
                    'Wrzesień',
                    'Październik',
                    'Listopad',
                    'Grudzień'
                ];
                return monthNames[monthNumber - 1];
            };
        }
    ).filter('weekDay', function() {

            // Użycie: {{ when | date 'EEE' | weekDay }}

            return function(weekDay) {
                var weekDays = {
                    "Mon": "Poniedziałek",
                    "Tue": "Wtorek",
                    "Wen": "Środa",
                    "Thu": "Czwartek",
                    "Fri": "Piątek",
                    "Sat": "Sobota",
                    "Sun": "Niedziela"
                };
                return weekDays[weekDay] || weekDay;
            };
        }
    );