var appServices = angular.module('appServices',['ngResource']);
appServices
    .factory('Gallery', ['$resource',
    function($resource){
        return $resource('json/galleries/:galleryId.json', {}, {
            query: {
                method:'GET',
                params:{galleryId:'galleries'},
                isArray:true
            }
        });
    }
]).factory('Comment', ['$resource',
    function($resource){
        return $resource('json/galleries/:galleryId-comments.json', {}, {
            get: {
                method:'GET',
                isArray:true
            }
        });
    }
]);
