$(document).ready(function () {
    $(".navbar-nav a").click(function (event) {
        event.preventDefault();
    });
    $('#stronaglownaScroll').click(function () {
        $('html, body').animate({
            scrollTop: $('#stronaglowna').offset().top
        }, 1000, 'swing');
        return false;
    });
    $('#omnieScroll').click(function () {
        $('html, body').animate({
            scrollTop: $('#omnie').offset().top
        }, 1000, 'swing');
        return false;
    });
    $('#poradnikiScroll').click(function () {
        $('html, body').animate({
            scrollTop: $('#poradniki').offset().top
        }, 1000, 'swing');
        return false;
    });
    $('#dopoczytaniaScroll').click(function () {
        $('html, body').animate({
            scrollTop: $('#dopoczytania').offset().top
        }, 1000, 'swing');
        return false;
    });
    $('#galeriaScroll').click(function () {
        $('html, body').animate({
            scrollTop: $('#galeria').offset().top
        }, 1000, 'swing');
        return false;
    });
    $('#kontaktScroll').click(function () {
        $('html, body').animate({
            scrollTop: $('#kontakt').offset().top
        }, 1000, 'swing');
        return false;
    });
});